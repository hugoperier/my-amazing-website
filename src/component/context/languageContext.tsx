import React, { useState, createContext, useContext } from 'react';

import { languageOptions, dictionaryList } from '../languages';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
console.log(cookies.get('languageId'))

export const LanguageContext = createContext({
    language: languageOptions[cookies.get('languageId') || 0],
    dictionary: dictionaryList[languageOptions[cookies.get('languageId') || 0].id]
});

export function LanguageProvider(props) {
    const languageContext = useContext(LanguageContext);
    const [language, setLanguage] = useState(languageContext.language);
    const [dictionary, setDictionary] = useState(languageContext.dictionary);
  
    const provider = {
      language,
      dictionary,
      setLanguage: (selectedLanguage) => {
        setLanguage(selectedLanguage);
        setDictionary(dictionaryList[selectedLanguage.id]);
      }
    };
  
    return (
      <LanguageContext.Provider value={provider}>
        {props.children}
      </LanguageContext.Provider>
    );
  };
  
  // get text according to id & current language
  export function Text(props) {
    const languageContext = useContext(LanguageContext);
  
    return languageContext.dictionary[props.tid];
  };
