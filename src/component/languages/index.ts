import fr from './fr';
import en from './en';

export const dictionaryList = {
  fr,
  en,
};

export const languageOptions = [
  { idx:0, id: 'fr', flagUrl: "https://twemoji.maxcdn.com/v/12.1.6/72x72/1f1e8-1f1f5.png", text: 'Francais' },
  { idx:1, id: 'en', flagUrl: "https://twemoji.maxcdn.com/v/12.1.6/72x72/1f1ec-1f1e7.png", text: 'English'},
];
