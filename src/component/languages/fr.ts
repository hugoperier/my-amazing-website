const fr = {
  _lang: "fr",

  /*
   *  Menu
   */
  menuHome: "Accueil",
  menuWhoIAm: "Qui suis-je ?",
  menuProject: "Mes projets",
  menuMyWay: "Mon parcours",
  menuSkill: "Mes competences",
  menuContactMe: "Me contacter",

  /*
   *  Home Page
   */
  titleMessage: "Vous venez de trouver votre future",
  titleLabel1: "Programmeur 💻",
  titleLabel2: "Freelance 📱",
  titleLabel3: "Super Stagiaire 🚀",
  titleLabel4: "Data Scientiste 📈",
  homeDescription:
    "Développez votre projet dans la bonne voie et avec passion ! Concentrez-vous sur vos idées et laissez moi faire la partie numérique",

  /*
   * Who Am I
   */
  occupation: "Etudiant",
  schoolPlace: "Epitech",
  selfDescription:
    "Étudiant en 4eme année au sein de l'école Epitech, je me suis passionné pour le développement logiciel, site internet ainsi que pour la data science et Big data. J'aime apprendre et j'aime les projets qui m'apportent des connaissances dans mon domaine ou des domaines m'étant inconnus. Passionné de science et plus particulièrement de physique et d'astronomie, j'espère pouvoir travailler dans un organisme à but scientifique ",
  myLanguage: "Mes langues",
  nativeLanguage: "Natale",
  professionalLanguage: "Professionnel",
  sportBasketBall: "Basket",
  sportSki: "Ski",
  sportSwimming: "Natation",
  hobby: "Centre d'interets",
  hobbyScience: "Science",
  hobbyInformatic: "Informatique",
  hobbyAstronomy: "Astronomie",
  hobbyCooking: "Cuisine",
  hobbyVideoGames: "Jeux vidéos",
  cvtitle: "Téléchargez mon CV !",

  /*
   * My way
   */

  january: "Janvier",
  march: "Mars",
  april: "Avril",
  july: "Juillet",
  september: "Septembre",
  december: "Decembre",
  epitekDegree: "Diplome de niveau 1 expert informatique",
  ggjDescription: "Developpement d'un jeu vidéo en 48h",
  sidoDescription:
    "Chercher et concevoir une solution à l'aide d'objets connectés",
  dataDevelopperLabel: "Data Developpeur",
  smartTraffikDescription:
    "Developpement C#, Azure Datalake, Python, SQL, Entity Framework",
  googleHashcodeDescription: "Compétition de programmation en équipe",
  nrjTitle: "Developpeur Full Stack",
  kuTitle: "Année à l'internationnal",
  kuDescription: "Développement logiciel, Big Data, Intelligence Artificielle",

  /*
   * My work
   */

  myWorkTitle: "Mes Projets personnels",
  dtPriceDescription:
    "Dt Price est un outil d'analyse des prix pour le jeu Dofus Touch. 1 an d'existence et environ 700 utilisateurs actifs",

  /*
   * My skills
   */

   skillTitle: "Mes compétences",
   skillSubTitle: "Je suis bon dans ces domaines",
   otherLabel: "Et d'autres",
   /*
   * Contact
   */

   contactTitle: "Me contacter",
   to: "À",
   from: "De",
   object: "Objet",
   send: "Envoyer",
   contactPlaceHolder: "Entrez votre message ici",

   statusMessageSuccess: "Le mail a bien été envoyé",
   statusMessageError1: "Veuillez remplir tout les champs",
   statusMessageError2: "Erreur, réessayez plus tard",
   statusMessageError3: "Veuillez remplir le captcha"
};

export default fr;
