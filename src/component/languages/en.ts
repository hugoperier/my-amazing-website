const en = {
  _lang: "en",


  menuHome: "Home",
  menuWhoIAm: "Who am I ?",
  menuProject: "My projects",
  menuMyWay: "Professional Development",
  menuSkill: "My skills",
  menuContactMe: "Contact me",

  /*
   *  Home Page
   */
  titleMessage: "You just have found your future",
  titleLabel1: "Programmer 💻",
  titleLabel2: "Freelance 📱",
  titleLabel3: "Internship 🚀",
  titleLabel4: "Data Scientist 📈",
  homeDescription:
    "Make your project grow in the best way and with passion ! Focus yourself on your idea and let me take care about the IT part",
  /*
   * Who Am I
   */
  occupation: "Student",
  schoolPlace: "Epitech",
  selfDescription:
    "Student in fourth year in the school Epitech, I'm passionate about software and website development as well as Data Science and Big Data. I like projects that bring me knowledge in my field or fields that are unknown to me. Passionate about science and more particularly physics and astronomy, I hope to work in an organization with a scientific purpose. ",
  myLanguage: "My languages",
  nativeLanguage: "Native",
  professionalLanguage: "Professionnal",
  sportBasketBall: "Basket",
  sportSki: "Skiing",
  sportSwimming: "Swimming",
  hobby: "Hobbys",
  hobbyScience: "Science",
  hobbyInformatic: "Computer Science",
  hobbyAstronomy: "Astronomy",
  hobbyCooking: "Cooking",
  hobbyVideoGames: "Video Games",
  cvtitle: "Download my CV !",

  /*
   * My way
   */

  january: "January",
  march: "March",
  april: "April",
  july: "July",
  september: "September",
  december: "December",
  epitekDegree: "5 year computer science degree",
  ggjDescription: "Making a videogame in 48 hours",
  sidoDescription: "Find and make a solution using IOT",
  dataDevelopperLabel: "Data Developer",
  smartTraffikDescription:
    "C# Development, Azure Datalake, Python, SQL, Entity Framework",
  googleHashcodeDescription: "Team Programming Competition",
  nrjTitle: "Full Stack developper",
  kuTitle: "International year",
  kuDescription: "Software Development, Big Data, Artificial Intelligence",

  /*
   * My work
   */

  myWorkTitle: "My side projects",
  dtPriceDescription:
    "Dt Price is a price analysis tool for the game Dofus Touch. 1 year of existence and about 700 active users",

  /*
   * My skills
   */

  skillTitle: "My skills",
  skillSubTitle: "I can say that I am good at",
  otherLabel: "And others",
  /*
   * Contact
   */

  contactTitle: "Contact me",
  to: "To",
  from: "From",
  object: "Object",
  send: "Send",
  contactPlaceHolder: "Type your message here",

  statusMessageSuccess: "The mail has been sended",
  statusMessageError1: "Please fill all the form",
  statusMessageError2: "Error, please try later",
  statusMessageError3: "Please fill the captcha"
};

export default en;
