import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Particles from "react-particles-js";
import Typical from "react-typical";
import { Element } from "react-scroll";

import ProgrammerImg from "../../../ressources/programmer2.jpg";
import Carb from "../../../ressources/carb.png";
import { Text } from "../../context/languageContext";

const useStyles = makeStyles({
  homeBackground: {
    backgroundImage: "linear-gradient(293deg, #01DBB0 0%, #6E49D9 100%)",
    width: "100%",
    height: "100%",
    position: "absolute",
    "@media (max-width: 900px)": {
      height: "80%",
    },
  },
  imgtest: {
    background: `url('${Carb}') bottom no-repeat`,
    backgroundSize: "contain",
  },
  particleCanvas: {
    position: "absolute",
    left: 0,
    top: "0%",
    width: "100%",
    height: "100%",
    zIndex: 3,
  },
  textTitle: {
    position: "relative",
    margin: "0 auto",
    marginBottom: "20px",
    color: "#fff",
    fontSize: "2.825em",
    lineHeight: "1.35",
    fontFamily: "'Baloo Tamma 2', cursive",
    zIndex: 5,
    "@media (max-width: 900px)": {
      fontSize: "1.2em",
    },
  },
  home: {
    paddingTop: "74px",
    position: "relative",
    textAlign: "center",
  },
  container: {
    width: "600px",
    position: "relative",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "60px",
    "@media (max-width: 900px)": {
      width: "200px",
    },
  },
  headerText: {
    color: "#fff",
    fontWeight: 300,
    fontSize: "1.375em",
    lineHeight: "1.7",
    display: "inline",
    zIndex: 5,
    position: "relative",
    "@media (max-width: 900px)": {
      fontSize: "11px",
    },
  },
  test: {
    marginTop: "5px",
  },
  emoji: {
    marginLeft: "10px",
    position: "relative",
    top: "3px",
    width: "24px",
    height: "24px",
    "@media (max-width: 900px)": {
      width: "12.5px",
      height: "12.5px",
      marginLeft: "4px",
    },
  },
  imgProgrammer: {
    paddingTop: "50px",
    zIndex: 5,
    position: "relative",
    "@media (max-width: 900px)": {
      width: "80%",
    },
  },
});

export default function Home() {
  const classes = useStyles();

  return (
    <Fragment>
      <Element name="home" className="home">
        <div className={classes.homeBackground}>
          <Particles
            className={classes.particleCanvas}
            params={{
              particles: {
                number: {
                  value: 80,
                  density: {
                    enable: true,
                    value_area: 800,
                  },
                },
                line_linked: {
                  enable: true,
                  distance: 150,
                  color: "#ffffff",
                  opacity: 0.4,
                  width: 1,
                },
                move: {
                  enable: true,
                  speed: 6,
                  direction: "none",
                  random: false,
                  straight: false,
                  out_mode: "out",
                  bounce: false,
                  attract: {
                    enable: false,
                    rotateX: 600,
                    rotateY: 1200,
                  },
                },
                shape: {
                  type: "circle",
                  polygon: {
                    nb_sides: 5,
                  },
                },
                color: {
                  value: "#CCC",
                },
                size: {
                  value: 3,
                  random: true,
                  anim: {
                    enable: false,
                    speed: 40,
                    size_min: 0.1,
                    sync: false,
                  },
                },
              },
              interactivity: {
                detect_on: "canvas",
                events: {
                  onhover: {
                    enable: true,
                    mode: "repulse",
                  },
                  onclick: {
                    enable: true,
                    mode: "push",
                  },
                  resize: true,
                },
                modes: {
                  grab: {
                    distance: 400,
                    line_linked: {
                      opacity: 1,
                    },
                  },
                  bubble: {
                    distance: 400,
                    size: 40,
                    duration: 2,
                    opacity: 8,
                  },
                  repulse: {
                    distance: 80,
                    duration: 0.4,
                  },
                  push: {
                    particles_nb: 4,
                  },
                  remove: {
                    particles_nb: 2,
                  },
                },
              },
              retina_detect: false,
            }}
          />
        </div>
        <GreetingText />
      </Element>
    </Fragment>
  );
}

const GreetingText = () => {
  const classes = useStyles();

  return (
    <div className={classes.home}>
      <div className={classes.imgtest}>
        <div className={classes.container}>
          <h1 className={classes.textTitle}>
            <span>
              <Text tid="titleMessage" />
            </span>
            <Typical
              className={classes.test}
              steps={[
                "Programmer 💻",
                1500,
                "Freelance 📱",
                1500,
                "Collaborater 🚀",
                1500,
                "Data Scientist 📈",
                1500,
              ]}
              loop={Infinity}
              wrapper="p"
            />
          </h1>
          <p className={classes.headerText}>
            <Text tid="homeDescription" />
          </p>
          <img
            className={classes.emoji}
            alt="emoji-dream"
            src="https://twemoji.maxcdn.com/v/12.1.5/72x72/1f929.png"
          />
        </div>
        <img
          className={classes.imgProgrammer}
          alt="programmer-img"
          src={ProgrammerImg}
        />
      </div>
    </div>
  );
};
