import React from "react";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import { makeStyles } from "@material-ui/core";
import SchoolIcon from "@material-ui/icons/School";
import ComputerIcon from "@material-ui/icons/Computer";
import WorkIcon from "@material-ui/icons/Work";
import { Element } from "react-scroll";
import { Text } from "../../context/languageContext";

const useStyles = makeStyles({
  background: {
    background: "#ededed",
    paddingTop: "50px",
    paddingBottom: "30px",
  },
  verticalTimeLineElement: {
    "& span": {
      color: "#333",
    },
  },
  title: {
    textAlign: "center",
    paddingTop: "30px",
    paddingBottom: "50px",
    "& h1": {
      marginTop: "0",
      color: "#333",
    },
  },
});

export default function MyWay() {
  const classes = useStyles();

  return (
    <Element name="my-way" className={classes.background}>
      <div className={classes.title}>
        <h1>
          <Text tid="menuMyWay" />
        </h1>
      </div>
      <VerticalTimeline>
        <VerticalTimelineElement
          className={classes.verticalTimeLineElement}
          contentStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
          contentArrowStyle={{ borderRight: "7px solid  rgb(33, 150, 243)" }}
          date="2016 - 2021"
          icon={<SchoolIcon />}
          iconStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
        >
          <h3 className="vertical-timeline-element-title">Epitech</h3>
          <p>
            <Text tid="epitekDegree" />
          </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className={classes.verticalTimeLineElement}
          date={`${Text({ tid: "january" })} 2017`}
          contentStyle={{ background: "#42f56f", color: "#fff" }}
          contentArrowStyle={{ borderRight: "7px solid  #42f56f" }}
          icon={<ComputerIcon />}
          iconStyle={{ background: "#42f56f", color: "#fff" }}
        >
          <h3 className="vertical-timeline-element-title">Global Game Jam</h3>
          <h4 className="vertical-timeline-element-subtitle">
            <Text tid="ggjDescription" />
          </h4>
          <p>Unity, C#</p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className={classes.verticalTimeLineElement}
          date={`${Text({ tid: "march" })} 2017`}
          contentStyle={{ background: "#42f56f", color: "#fff" }}
          contentArrowStyle={{ borderRight: "7px solid  #42f56f" }}
          icon={<ComputerIcon />}
          iconStyle={{ background: "#42f56f", color: "#fff" }}
        >
          <h3 className="vertical-timeline-element-title">Hackathon Sido</h3>
          <p>
            <Text tid="sidoDescription" />
          </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className={classes.verticalTimeLineElement}
          date={`${Text({ tid: "july" })} 2017 - ${Text({
            tid: "december",
          })} 2017`}
          contentStyle={{ background: "rgb(233, 30, 99)", color: "#fff" }}
          contentArrowStyle={{ borderRight: "7px solid  rgb(233, 30, 99)" }}
          icon={<WorkIcon />}
          iconStyle={{ background: "rgb(233, 30, 99)", color: "#fff" }}
        >
          <h3 className="vertical-timeline-element-title">
            <Text tid="dataDevelopperLabel" />
          </h3>
          <h4 className="vertical-timeline-element-subtitle">
            Smart Traffik, Lyon, FR
          </h4>
          <p>
            <Text tid="smartTraffikDescription" />
          </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className={classes.verticalTimeLineElement}
          date={`${Text({ tid: "january" })} 2018`}
          contentStyle={{ background: "#42f56f", color: "#fff" }}
          contentArrowStyle={{ borderRight: "7px solid  #42f56f" }}
          icon={<ComputerIcon />}
          iconStyle={{ background: "#42f56f", color: "#fff" }}
        >
          <h3 className="vertical-timeline-element-title">Global Game Jam</h3>
          <h4 className="vertical-timeline-element-subtitle">
            <Text tid="ggjDescription" />
          </h4>
          <p>Unity, C#</p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className={classes.verticalTimeLineElement}
          date={`${Text({ tid: "april" })} 2018`}
          contentStyle={{ background: "#42f56f", color: "#fff" }}
          contentArrowStyle={{ borderRight: "7px solid  #42f56f" }}
          icon={<ComputerIcon />}
          iconStyle={{ background: "#42f56f", color: "#fff" }}
        >
          <h3 className="vertical-timeline-element-title">Google Hashcode</h3>
          <h4 className="vertical-timeline-element-subtitle">Hackathon</h4>
          <p>
            <Text tid="googleHashcodeDescription" />
          </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className={classes.verticalTimeLineElement}
          date={`${Text({ tid: "january" })} 2019`}
          contentStyle={{ background: "#42f56f", color: "#fff" }}
          contentArrowStyle={{ borderRight: "7px solid  #42f56f" }}
          icon={<ComputerIcon />}
          iconStyle={{ background: "#42f56f", color: "#fff" }}
        >
          <h3 className="vertical-timeline-element-title">Global Game Jam</h3>
          <h4 className="vertical-timeline-element-subtitle">
            <Text tid="ggjDescription" />
          </h4>
          <p>Unity, C#</p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className={classes.verticalTimeLineElement}
          date={`${Text({ tid: "march" })} 2019 - ${Text({
            tid: "july",
          })} 2019`}
          contentStyle={{ background: "rgb(233, 30, 99)", color: "#fff" }}
          contentArrowStyle={{ borderRight: "7px solid  rgb(233, 30, 99)" }}
          icon={<WorkIcon />}
          iconStyle={{ background: "rgb(233, 30, 99)", color: "#fff" }}
        >
          <h3 className="vertical-timeline-element-title">
            <Text tid="nrjTitle" />
          </h3>
          <h4 className="vertical-timeline-element-subtitle">
            NRJ Global Regions, Lyon, FR
          </h4>
          <p>C#, Asp .Net Core, Entity Framework, REST, Javascript, UX</p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className={classes.verticalTimeLineElement}
          date={`${Text({ tid: "september" })} 2019 - ${Text({
            tid: "july",
          })} 2020`}
          contentStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
          contentArrowStyle={{ borderRight: "7px solid  rgb(33, 150, 243)" }}
          icon={<SchoolIcon />}
          iconStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
        >
          <h3 className="vertical-timeline-element-title">Korea University</h3>
          <h4 className="vertical-timeline-element-subtitle">
            <Text tid="kuTitle" />
          </h4>
          <p>
            <Text tid="kuDescription" />
          </p>
        </VerticalTimelineElement>
      </VerticalTimeline>
    </Element>
  );
}
