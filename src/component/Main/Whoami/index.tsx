import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Hugo from "../../../ressources/hugo.jpg";
import { Grid, Paper } from "@material-ui/core";
import PersonIcon from "@material-ui/icons/Person";
import WorkIcon from "@material-ui/icons/Work";
import SchoolIcon from "@material-ui/icons/School";
import PublicIcon from "@material-ui/icons/Public";
import { Element } from "react-scroll";
import { Text } from "../../context/languageContext";
import GetAppIcon from "@material-ui/icons/GetApp";

const CvFr = require("../../../ressources/CV_fr.pdf");
const CvEn = require("../../../ressources/CV_en.pdf")

const useStyles = makeStyles({
  background: {
    backgroundImage: "linear-gradient(293deg, #01DBB0 0%, #6E49D9 100%)",
    width: "100%",
    marginTop: "70px",
    position: "relative",
    height: "100%",
    "@media (max-width: 900px)": {
      height: "auto",
    },
  },
  imgHugo: {
    position: "relative",
    transform: "translateY(-50%)",
    top: "40%",
    marginLeft: "37px",
    borderRadius: "12%",
    border: "solid 8px aliceblue",
    width: "20%",
    "@media (max-width: 900px)": {
      width: "50%",
      marginLeft: "22%",
      marginRight: "22%",
      marginTop: "50px",
      top: 0,
      transform: "translateY(0%)",
    },
  },
  infoPanelContainer: {
    paddingTop: "50px",
    position: "relative",
    width: "50%",
    float: "right",
    marginRight: "10%",
    "@media (max-width: 900px)": {
      float: "none",
      width: "80%",
      marginRight: "10%",
      marginLeft: "10%",
      paddingBottom: "40px",
    },
  },
  titleContainer: {
    textAlign: "center",
    paddingTop: "10px",
    "@media (max-width: 900px)": {
      fontSize: "0.85em",
    },
  },
  persoInfoBox: {
    "& span": {
      display: "flex",
      "& p": {
        display: "contents",
        fontSize: "1.275em",
      },
    },
    "@media (max-width: 900px)": {
      "& p": {
        fontSize: "0.95em !important",
      },
      "& svg": {
        height: "20px",
        width: "20px",
      },
    },
  },
  bio: {
    "@media (max-width: 900px)": {
      "& p": {
        fontSize: "11px",
      },
    },
  },
  objectives: {
    "@media (max-width: 900px)": {
      "& p": {
        fontSize: "11px",
      },
    },
  },
  langContainer: {
    display: "flex",
    "& img": {
      width: "24px",
      height: "24px",
    },
    "& p": {
      marginTop: "4px",
      marginLeft: "10px",
    },
  },
  containerFirstBox: {
    marginLeft: "20px",
    paddingRight: "50px",
  },
  containerSecondBox: {
    "@media (max-width: 900px)": {
      marginLeft: "20px",
      paddingRight: "50px",
    },
  },
  containerElements: {
    "@media (max-width: 900px)": {
      display: "block",
      "& h2": {
        fontSize: "1.25em",
      },
    },
    display: "flex",
    "& h2": {
      color: "#6E49D9",
    },
    "& svg": {
      color: "#6E49D9",
    },
  },
  imgContainer: {
    marginRight: "20px",
    textAlign: "center",
    "& img": {
      borderRadius: "50%",
    },
    "& p": {
      position: "relative",
      bottom: "15px",
    },
    "@media (max-width: 900px)": {
      "& img": {
        height: "35px",
      },
      "& p": {
        fontSize: "11px",
      },
    },
  },
  downloadCvContainer: {
    position: "absolute",
  },
  cvIconDownload: {
    fontSize: "2.5rem",
  },
  cv: {
    position: "relative",
    bottom: "60px",
    left: "20px",
  },
});

export default function Whoami() {
  const classes = useStyles();

  return (
    <Element name="whoami" className={classes.background}>
      <img alt="hugo" src={Hugo} className={classes.imgHugo} />
      <div className={classes.infoPanelContainer}>
        <PresentationPaperCard />
      </div>
    </Element>
  );
}

const PresentationPaperCard = () => {
  const classes = useStyles();
  const cvFile = Text({tid:'_lang'}) === "fr" ? CvFr : CvEn

  return (
    <Paper elevation={3}>
      <div className={classes.titleContainer}>
        <h1>Hugo PERIER</h1>
        <div className={classes.downloadCvContainer}>
          <div className={classes.cv}>
            <a href={cvFile} download title={Text({ tid: "cvtitle" })}>
              <GetAppIcon className={classes.cvIconDownload} />
            </a>
          </div>
        </div>
      </div>
      <div className={classes.containerElements}>
        <Grid
          container
          direction="column"
          justify="flex-start"
          alignItems="flex-start"
          className={classes.containerFirstBox}
        >
          <Grid item className={classes.persoInfoBox}>
            <span>
              <PersonIcon />
              <p>22</p>
            </span>
            <span>
              <PublicIcon />
              <p>Lyon, France</p>
            </span>
            <span>
              <WorkIcon />
              <p>
                <Text tid="occupation" />
              </p>
            </span>
            <span>
              <SchoolIcon />
              <p>Epitech</p>
            </span>
          </Grid>
          <Grid item /* Bio */ className={classes.bio}>
            <h2>Bio</h2>
            <p>
              <Text tid="selfDescription" />
            </p>
          </Grid>
          <Grid item /* objectifs*/ className={classes.objectives}>
            <h2>
              <Text tid="myLanguage" />
            </h2>
            <div className={classes.langContainer}>
              <img
                src="https://twemoji.maxcdn.com/v/12.1.6/72x72/1f1f2-1f1eb.png"
                alt="fr-flg"
              />
              <p>
                <Text tid="nativeLanguage" />
              </p>
            </div>
            <div className={classes.langContainer}>
              <img
                src="https://twemoji.maxcdn.com/v/12.1.6/72x72/1f1ec-1f1e7.png"
                alt="en-flg"
              />
              <p>
                <Text tid="professionalLanguage" />
              </p>
            </div>
          </Grid>
        </Grid>
        <Grid
          container
          direction="column"
          justify="flex-start"
          alignItems="flex-start"
          className={classes.containerSecondBox}
        >
          <Grid item /*sport*/>
            <h2>Sports</h2>

            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
            >
              <Grid item className={classes.imgContainer}>
                <img
                  alt="basketball"
                  src="https://twemoji.maxcdn.com/v/12.1.5/72x72/1f3c0.png"
                />
                <p>
                  <Text tid="sportBasketBall" />
                </p>
              </Grid>
              <Grid item className={classes.imgContainer}>
                <img
                  alt="ski"
                  src="https://twemoji.maxcdn.com/v/12.1.5/72x72/26f7-1f3fc.png"
                />
                <p>
                  <Text tid="sportSki" />
                </p>
              </Grid>
              <Grid item className={classes.imgContainer}>
                <img
                  alt="nage"
                  src="https://twemoji.maxcdn.com/v/12.1.5/72x72/1f3ca-1f3fb-200d-2642-fe0f.png"
                />
                <p>
                  <Text tid="sportSwimming" />
                </p>
              </Grid>
            </Grid>
          </Grid>
          <Grid item /* passion */>
            <h2>
              <Text tid="hobby" />
            </h2>

            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
            >
              <Grid item className={classes.imgContainer}>
                <img
                  alt="science"
                  src="https://twemoji.maxcdn.com/v/12.1.5/72x72/1f468-1f3fb-200d-1f52c.png"
                />
                <p>
                  <Text tid="hobbyScience" />
                </p>
              </Grid>
              <Grid item className={classes.imgContainer}>
                <img
                  alt="Informatique"
                  src="https://twemoji.maxcdn.com/v/12.1.5/72x72/1f4bb.png"
                />
                <p>
                  <Text tid="hobbyInformatic" />
                </p>
              </Grid>
              <Grid item className={classes.imgContainer}>
                <img
                  alt="astronomie"
                  src="https://twemoji.maxcdn.com/v/12.1.5/72x72/1f320.png"
                />
                <p>
                  <Text tid="hobbyAstronomy" />
                </p>
              </Grid>
              <Grid item className={classes.imgContainer}>
                <img
                  alt="cuisine"
                  src="https://twemoji.maxcdn.com/v/12.1.5/72x72/1f468-1f3fb-200d-1f373.png"
                />
                <p>
                  <Text tid="hobbyCooking" />
                </p>
              </Grid>
              <Grid item className={classes.imgContainer}>
                <img
                  alt="videogames"
                  src="https://twemoji.maxcdn.com/v/12.1.5/72x72/1f3ae.png"
                />
                <p>
                  <Text tid="hobbyVideoGames" />
                </p>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </Paper>
  );
};
