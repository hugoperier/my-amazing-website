import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Paper, Grid } from "@material-ui/core";
import JavascriptLogo from "../../../ressources/javascript.svg";
import CshapLogo from "../../../ressources/c-sharp.svg";
import PythonLogo from "../../../ressources/python.svg";
import OtherLogo from "../../../ressources/dots.svg";
import useWindowDimensions from "../../hooks/useWindowsDimension";
import {Element} from 'react-scroll'
import { Text } from "../../context/languageContext";

const useStyles = makeStyles({
  skillsContainer: {
    width: "98%",
    paddingBottom: "80px",
    paddingTop: "70px"
  },
  title: {
    textAlign: "center",
    "& h1": {
      color: "rgb(40, 0, 104)",
      marginBottom: "0px",
    },
    "@media (min-width: 900px)": {
      "& p": {
        paddingBottom: "50px",
      },
    },
  },
  paperCard: {},
  paperContainer: {
    textAlign: "center",
    paddingTop: "20px",
    paddingRight: "24px",
    paddingLeft: "24px",
    paddingBottom: "20px",
    "& img": {
      width: "100px",
    },
  },
  item: {
    width: "22%",
    minWidth: "200px",
    maxWidth: "400px",
    "@media (max-width: 900px)": {
      transform: "scale(0.7, 0.7)",
    },
  },
});

export default function Skills() {
  const classes = useStyles();
  const { width } = useWindowDimensions();

  return (
       <Element name="my-skill" className={classes.skillsContainer}>
      <div className={classes.title}>
        <h1><Text tid="skillTitle" /></h1>
        <p><Text tid="skillSubTitle" /></p>
      </div>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={width && width > 900 ? 5 : 0}
      >
        <Grid item className={classes.item} xs={12} md={3}>
          <CsCard />
        </Grid>
        <Grid item className={classes.item} xs={12} md={3}>
          <JsCard />
        </Grid>
        <Grid item className={classes.item} xs={12} md={3}>
          <PythonCard />
        </Grid>
        <Grid item className={classes.item} xs={12} md={3}>
          <OthersCard />
        </Grid>
      </Grid>
      </Element>
  );
}

const JsCard = () => {
  const classes = useStyles();

  return (
    <Fragment>
      <Paper elevation={3} className={classes.paperCard}>
        <div className={classes.paperContainer}>
          <img src={JavascriptLogo} alt="js-logo" />
          <h2 style={{ color: "#f7df1e" }}>Javascript</h2>
          <div>Typescript</div>
          <div>React</div>
          <div>Nodejs</div>
          <div>ES6</div>
          <div>JQuery</div>
        </div>
      </Paper>
    </Fragment>
  );
};

const CsCard = () => {
  const classes = useStyles();

  return (
    <Fragment>
      <Paper elevation={3} className={classes.paperCard}>
        <div className={classes.paperContainer}>
          <img src={CshapLogo} alt="cs-logo" />
          <h2 style={{ color: "#280068" }}>C#</h2>
          <div>Asp .Net Core</div>
          <div>Entity Framework</div>
          <div>Razzor</div>
          <div>Unity</div>
        </div>
      </Paper>
    </Fragment>
  );
};

const PythonCard = () => {
  const classes = useStyles();

  return (
    <Fragment>
      <Paper elevation={3} className={classes.paperCard}>
        <div className={classes.paperContainer}>
          <img src={PythonLogo} alt="python-logo" />
          <h2 style={{ color: "#3770a0" }}>Python</h2>
          <div>Script</div>
          <div>Tensorflow</div>
          <div>Keras</div>
          <div>Numpy</div>
          <div>Panda</div>
        </div>
      </Paper>
    </Fragment>
  );
};

const OthersCard = () => {
  const classes = useStyles();

  return (
    <Fragment>
      <Paper elevation={3} className={classes.paperCard}>
        <div className={classes.paperContainer}>
          <img src={OtherLogo} alt="dots-logo" />
          <h2 style={{ color: "#3770a0" }}><Text tid={"otherLabel"} /></h2>
          <div>C/C++</div>
          <div>MySql, SqlServer, MongoDB</div>
          <div>Devops, CI/CD Pipelines</div>
          <div>Html5/Css3</div>
          <div>Linux</div>
        </div>
      </Paper>
    </Fragment>
  );
};
