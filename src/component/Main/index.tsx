import React from "react";
import MenuBar from "./Menu";
import Home from "./Home";
import Whoami from "./Whoami";
import MyWay from "./MyWay";
import MyWork from "./MyWork";
import Skills from "./Skills";
import Contact from "./Contact";
import { LanguageProvider } from "../context/languageContext";

export default function Main() {
  return (
    <div>
      <LanguageProvider>
        <MenuBar />
        <Home />
        <Whoami />
        <MyWay />
        <MyWork />
        <Skills />
        <Contact />
      </LanguageProvider>
    </div>
  );
}
