import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Paper,
  FormControl,
  InputLabel,
  OutlinedInput,
  Button,
} from "@material-ui/core";
import { Element } from "react-scroll";
import { Text } from "../../context/languageContext";
import emailjs from "emailjs-com";
import ReCAPTCHA from "react-google-recaptcha";

const useStyles = makeStyles({
  background: {
    position: "relative",
    background: "linear-gradient(293deg, #01DBB0 0%, #6E49D9 100%)",
    paddingBottom: "200px",
    paddingTop: "50px",
  },
  title: {
    color: "#fff",
    textAlign: "center",
    paddingBottom: "30px",
    paddingTop: "25px",
    "@media (max-width: 900px)": {
      fontSize: "0.75em",
      paddingBottom: "60px",
    },
  },
  paperFormContainer: {
    width: "60%",
    marginLeft: "auto",
    marginRight: "auto",
    "@media (max-width: 900px)": {
      width: "90%",
    },
  },
  inputTextDisabled: {
    background: "rgba(220,220,220, 0.7)",
  },
  form: {
    width: "70%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  paper: {
    paddingTop: "40px",
    paddingBottom: "150px",
  },
  formControl: {
    paddingBottom: "20px",
  },
  textarea: {
    width: "100%",
    resize: "vertical",
    height: "200px",
    fontSize: "1rem",
    marginBottom: "30px",
  },
  btnContainer: {
    float: "right",
  },
  statusMessage: {
    position: "absolute",
    transition: "all .5s ease",
  },
  recaptcha: {
    "@media (max-width: 900px)": {
      transform: "scale(0.7, 0.7)",
      paddingTop: "50px"
    },
  }
});

export default function Contact() {
  const classes = useStyles();
  const [senderEmail, setSenderEmail] = useState("");
  const [emailObject, setEmailObject] = useState("");
  const [emailBody, setEmailBody] = useState("");
  const [statusMessage, setStatusMessage] = useState("");
  const [authentified, setAuthentified] = useState(false);
  const [msgStyle, setMsgStyle] = useState({
    transform: "scale(0, 0)",
    color: "black",
  });
  const statusSuccess = Text({ tid: "statusMessageSuccess" });
  const statusError1 = Text({ tid: "statusMessageError1" });
  const statusError2 = Text({ tid: "statusMessageError2" });
  const statusError3 = Text({ tid: "statusMessageError3" });

  const handleBtnClick = async (e) => {
    e.preventDefault();
    if (!authentified) {
      setStatusMessage(statusError3);
      setMsgStyle({ transform: "scale(1, 1)", color: "red" });
      window.setTimeout(() => {
        setMsgStyle({ transform: "scale(0, 0)", color: "black" });
      }, 4000);
    } else if (senderEmail !== "" && emailObject !== "" && emailBody !== "") {
      const returnValue = await sendEmail(e);
      if (returnValue) {
        setStatusMessage(statusSuccess);
        setSenderEmail("");
        setEmailBody("");
        setEmailObject("");
        setMsgStyle({ transform: "scale(1, 1)", color: "green" });
        window.setTimeout(() => {
          setMsgStyle({ transform: "scale(0, 0)", color: "black" });
        }, 4000);
      } else {
        setStatusMessage(statusError2);
        setMsgStyle({ transform: "scale(1, 1)", color: "red" });
        window.setTimeout(() => {
          setMsgStyle({ transform: "scale(0, 0)", color: "black" });
        }, 4000);
      }
    } else {
      setStatusMessage(statusError1);
      setMsgStyle({ transform: "scale(1, 1)", color: "red" });
      window.setTimeout(() => {
        setMsgStyle({ transform: "scale(0, 0)", color: "black" });
      }, 4000);
    }
  };

  const verifyCaptcha = (response) => {
    if (response) {
      setAuthentified(true);
    }
  };

  const handleEmailChange = (value) => {
    setSenderEmail(value);
  };

  const handleEmailObjectChange = (value) => {
    setEmailObject(value);
  };

  const handleEmailBodyChange = (value) => {
    setEmailBody(value);
  };

  const textPlaceHolder = Text({ tid: "contactPlaceHolder" });

  return (
    <Element name="contact" className={classes.background}>
      <div className={classes.title}>
        <h1>
          <Text tid="contactTitle" />
        </h1>
      </div>
      <div className={classes.paperFormContainer}>
        <Paper elevation={3} className={classes.paper}>
          <div className={classes.form}>
            <form onSubmit={handleBtnClick}>
              <FormControl
                fullWidth
                variant="outlined"
                className={classes.formControl}
              >
                <InputLabel htmlFor="component-outlined">
                  <Text tid="to" />
                </InputLabel>
                <OutlinedInput
                  disabled
                  defaultValue="hugo.perier@epitech.eu"
                  id="component-outlined"
                  label="to"
                  fullWidth
                  classes={{ root: classes.inputTextDisabled }}
                />
              </FormControl>
              <FormControl
                fullWidth
                variant="outlined"
                className={classes.formControl}
                required
              >
                <InputLabel htmlFor="component-outlined">
                  <Text tid="from" />
                </InputLabel>
                <OutlinedInput
                  id="component-outlined"
                  name="senderEmail"
                  onChange={(event) => handleEmailChange(event.target.value)}
                  label="From"
                  value={senderEmail}
                  fullWidth
                />
              </FormControl>
              <FormControl
                variant="outlined"
                className={classes.formControl}
                required
              >
                <InputLabel htmlFor="component-outlined">
                  <Text tid="object" />
                </InputLabel>
                <OutlinedInput
                  id="component-outlined"
                  name="object"
                  value={emailObject}
                  onChange={(event) =>
                    handleEmailObjectChange(event.target.value)
                  }
                  label="Object"
                  fullWidth
                />
              </FormControl>
              <textarea
                placeholder={textPlaceHolder}
                name="body"
                value={emailBody}
                className={classes.textarea}
                onChange={(event) => handleEmailBodyChange(event.target.value)}
              />              
              <div className={classes.btnContainer}>
                <Button variant="contained" type="submit" color="primary">
                  <Text tid="send" />
                </Button>
              </div>
              <ReCAPTCHA
                className={classes.recaptcha}              
                sitekey="6Lfid_UUAAAAAGTA4qNtGMkjbShzHWKdwztc6JYU"
                onChange={verifyCaptcha}
              />
              <div className={classes.statusMessage} style={msgStyle}>
                <p>{statusMessage}</p>
              </div>
            </form>
          </div>
        </Paper>
      </div>
    </Element>
  );
}

function sendEmail(e) {
  return new Promise((resolve, rejects) => {
    emailjs
      .sendForm(
        "gmail",
        "template_dxVb5Q5H",
        e.target,
        "user_RPpO6IfXpifslAvHtotX7"
      )
      .then(
        (result) => {
          console.log(result.text);
          resolve(true);
        },
        (error) => {
          console.log(error.text);
          resolve(false);
        }
      );
  });
}
