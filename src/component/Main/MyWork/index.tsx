import React from "react";
import { makeStyles } from "@material-ui/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import DtPrice from "../../../ressources/dtprice.png";
import { Grid } from "@material-ui/core";
import {Element} from 'react-scroll'
import { Text } from "../../context/languageContext";

const useStyles = makeStyles({
  background: {
    position: "relative",
    background: "linear-gradient(293deg, #01DBB0 0%, #6E49D9 100%)",
    paddingBottom: "80px",
    paddingTop: "20px"
  },
  root: {
    maxWidth: 345,
    "@media (max-width: 900px)": {
        transform: "scale(0.8, 0.8)"
    }
  },
  cardFooter: {
    "& h4": {
        paddingRight: "30px",
        marginLeft: "10px",
        color: "#00157f"
    }
  },
  media: {
    height: 165,
  },
  title: {
    color: "#fff",
    textAlign: "center",
    paddingBottom: "50px",
    paddingTop: "60px",
    "@media (max-width: 900px)": {
        fontSize: "0.75em",
        paddingBottom: "10px"
    }
  },
});

export default function MyWork() {
  const classes = useStyles();

  return (
      <Element name="my-project" className={classes.background} >
      <div className={classes.title}>
        <h1><Text tid="myWorkTitle" /></h1>
      </div>      
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item>
          <Card className={classes.root}>
            <CardActionArea  onClick={() => window.location.href="https://dev.dt-price.com"}>
              <CardMedia
                className={classes.media}
                image={DtPrice}
                title="Dt-Price"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Dt Price
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                <Text tid="dtPriceDescription" />
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions className={classes.cardFooter}>
              <h4>ReactJs</h4>
              <h4>NodeJs</h4>
              <h4>MySql</h4>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
      </Element>
  );
}
