import React, { useEffect, useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Button } from "@material-ui/core";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import useWindowDimensions from "../../hooks/useWindowsDimension";
import MobileMenu from "./MobileMenu";
import { Link } from "react-scroll";
import { Fragment } from "react";
import { languageOptions } from '../../languages';
import { LanguageContext, Text } from '../../context/languageContext';
import Cookies from 'universal-cookie';

const cookies = new Cookies();


const useStyles = makeStyles({
  navBar: {
    background: "transparent",
    minHeight: "100px",
    zIndex: 999,
    padding: "29px 0",
    position: "fixed",
    transition: "all .5s ease",
    "& span": {
      color: "#fff",
      position: "relative",
      cursor: "pointer",
      "&::after": {
        position: "absolute",
        left: "0",
        bottom: "0",
        width: "100%",
        height: "2px",
        content: "''",
        background: "#fff",
        top: "30px",
        transition: "all .5s ease",
        transform: "scale(0)",
      },
      "&:hover": {
        "&::after": {
          transform: "scale(1)",
        },
      },
    },
  },
  navBarDetached: {
    transition: "all .5s ease",
    padding: "5px 0",
    background: "#fff",
    "& span": {
      color: "#333",
      "&::after": {
        background: "#333",
      },
    },
  },
  selected: {
    "& span": {
      "&::after": {
        transform: "scale(1) !important",
      },
    },
  },
  languageContainer: {
    transition: "all .3s ease",
    position: "fixed",
    left: "20px",
    padding: "25px 0",
    zIndex: 1000,
    "& img": {
      width: "20px",
      paddingRight: "5px",
    },
  },
  languageContainerDetached: {
    transition: "all .3s ease",
    padding: "15px 0",
  },
  menuItem: {
    "& img": {
      width: "20px",
      paddingRight: "5px"
    }
  }
});

export default function MenuBar() {
  const { width } = useWindowDimensions();
  if (width && width > 900) {
    return <BigMenu />;
  }
  return <MobileMenu />;
}

const BigMenu = () => {
  const classes = useStyles();
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    window.onscroll = () => {
      setOffset(window.pageYOffset);
    };
  }, []);

  return (
    <div>
      <div
        className={
          `${classes.languageContainer}` +
          (offset > 0 ? ` ${classes.languageContainerDetached}` : ``)
        }
      >
        <LanguageSelect />
      </div>
      <Grid
        className={
          `${classes.navBar}` + (offset > 0 ? ` ${classes.navBarDetached}` : "")
        }
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={5}
      >
        <Grid item>
          <Link
            activeClass={classes.selected}
            to="home"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <span><Text tid="menuHome" /></span>
          </Link>
        </Grid>
        <Grid item>
          <Link
            activeClass={classes.selected}
            to="whoami"
            spy={true}
            smooth={true}
            duration={1000}
            offset={-50}
          >
            <span><Text tid="menuWhoIAm" /></span>
          </Link>
        </Grid>
        <Grid item>
          <Link
            activeClass={classes.selected}
            to="my-way"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <span><Text tid="menuMyWay" /></span>
          </Link>
        </Grid>
        <Grid item>
          <Link
            activeClass={classes.selected}
            to="my-project"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <span><Text tid="menuProject" /></span>
          </Link>
        </Grid>
        <Grid item>
          <Link
            activeClass={classes.selected}
            to="my-skill"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <span><Text tid="menuSkill" /></span>
          </Link>
        </Grid>
        <Grid item>
          <Link
            activeClass={classes.selected}
            to="contact"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <span><Text tid="menuContactMe" /></span>
          </Link>
        </Grid>
      </Grid>
    </div>
  );
};

const LanguageSelect = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const languageContext = useContext<any>(LanguageContext)

  const classes = useStyles()
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLanguageSelection = (event: any, lang: string) => {
    event.stopPropagation()
    const selectedLanguage = languageOptions.find(item => item.id === lang);
    cookies.set('languageId', selectedLanguage?.idx)
    languageContext.setLanguage(selectedLanguage)
    handleClose()
  }

  return (
    <Fragment>
      <Button
        aria-controls="customized-menu"
        aria-haspopup="true"
        variant="contained"
        color="primary"
        onClick={handleClick}
      >
        <img
          src={languageContext.language.flagUrl}
          alt="flag-lang"
        />
        {languageContext.language.text}
      </Button>
      <Menu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {languageOptions.map(item => (
          <MenuItem key={item.id} className={classes.menuItem} onClick={(e) => handleLanguageSelection(e, item.id)}>
            <img 
              src={item.flagUrl}
              alt={`flag-${item.id}`}
            />
            {item.text}
          </MenuItem>
        ))}
      </Menu>
    </Fragment>
  );
};
