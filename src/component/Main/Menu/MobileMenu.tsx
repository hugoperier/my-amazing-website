import React, { Fragment, useState, useEffect, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuIcon from "@material-ui/icons/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { Link } from "react-scroll";
import { languageOptions } from '../../languages';
import { LanguageContext, Text } from '../../context/languageContext';

import Cookies from 'universal-cookie';

const cookies = new Cookies();

const useStyles = makeStyles({
  navBar: {
    background: "transparent",
    height: "100px",
    width: "100%",
    zIndex: 999,
    position: "fixed",
    transition: "all .5s ease",
  },
  navBarDetached: {
    transition: "all .5s ease",
    padding: "5px 0",
    height: "50px !important",
    width: "100%",
    background: "#fff",
  },
  btnRootColored: {
    background: "#6E49D9",
    color: "white",
    float: "right",
    marginRight: "20px",
    position: "relative",
    transform: "translateY(-50%)",
    top: "50%",
  },
  btnRootTransparent: {
    background: "transparent",
    border: "1px solid white",
    color: "white",
    float: "right",
    marginRight: "20px",
    position: "relative",
    transform: "translateY(-50%)",
    top: "50%",
  },
  menuItem: {
    "& img": {
      width: "20px",
      paddingRight: "5px"
    }
  },
  languageContainer: {
    float: "left",
    marginLeft: "20px",
    position: "relative",
    transform: "translateY(-50%)",
    top: "50%",
    "& img": {
      width: "20px",
      paddingRight: "5px",
    },
  },
  languageContainerDetached: {
    background: "transparent",
    float: "left",
    marginLeft: "20px",
    position: "relative",
    transform: "translateY(-50%)",
    top: "50%",
  },
});

export default function MobileMenu() {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const classes = useStyles();
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    window.onscroll = () => {
      setOffset(window.pageYOffset);
    };
  }, []);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div
      className={
        `${classes.navBar}` + (offset > 0 ? ` ${classes.navBarDetached}` : "")
      }
      >
      <div
        className={
          `${classes.languageContainer}` +
          (offset > 0 ? ` ${classes.languageContainerDetached}` : ``)
        }
      >
        <LanguageSelectMobile />
      </div>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
        classes={{
          root:
            offset > 0 ? classes.btnRootColored : classes.btnRootTransparent,
        }}
      >
        <MenuIcon />
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <Link
          to="home"
          spy={true}
          smooth={true}
          duration={1000}
        >
          <MenuItem onClick={handleClose}><Text tid={"menuHome"} /></MenuItem>
        </Link>

        <Link
            to="whoami"
            spy={true}
            smooth={true}
            duration={1000}
            offset={-50}
          >
        <MenuItem onClick={handleClose}><Text tid={"menuWhoIAm"} /></MenuItem>
        </Link>

        <Link
            to="my-way"
            spy={true}
            smooth={true}
            duration={1000}
          >
        <MenuItem onClick={handleClose}><Text tid={"menuMyWay"} /></MenuItem>
        </Link>

        <Link
            to="my-project"
            spy={true}
            smooth={true}
            duration={1000}
          >
        <MenuItem onClick={handleClose}><Text tid={"menuProject"} /></MenuItem>
        </Link>

        <Link
            to="my-skill"
            spy={true}
            smooth={true}
            duration={1000}
          >
        <MenuItem onClick={handleClose}><Text tid={"menuSkill"} /></MenuItem>
        </Link>

        <Link
            to="contact"
            spy={true}
            smooth={true}
            duration={1000}
          >
        <MenuItem onClick={handleClose}><Text tid={"menuContactMe"} /></MenuItem>
        </Link>
      </Menu>
    </div>
  );
}

const LanguageSelectMobile = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const languageContext = useContext<any>(LanguageContext)

  const classes = useStyles()
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLanguageSelection = (event: any, lang: string) => {
    event.stopPropagation()
    const selectedLanguage = languageOptions.find(item => item.id === lang);
    cookies.set('languageId', selectedLanguage?.idx)
    languageContext.setLanguage(selectedLanguage)
    handleClose()
  }

  return (
    <Fragment>
      <Button
        aria-controls="customized-menu"
        aria-haspopup="true"
        variant="contained"
        color="primary"
        onClick={handleClick}
      >
        <img
          src={languageContext.language.flagUrl}
          alt="flag-lang"
        />
        {languageContext.language.text}
      </Button>
      <Menu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {languageOptions.map(item => (
          <MenuItem key={item.id} className={classes.menuItem} onClick={(e) => handleLanguageSelection(e, item.id)}>
            <img 
              src={item.flagUrl}
              alt={`flag-${item.id}`}
            />
            {item.text}
          </MenuItem>
        ))}
      </Menu>
    </Fragment>
  );
};
